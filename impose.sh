#!/bin/bash
PAGES=`pdftk $1 dump_data | grep NumberOfPages | cut -f 2 -d" "`
FILES=

echo "imposing $1 ($PAGES pages)"

mkdir building

#split pdf into pages
pdftk $1 burst output building/pg_%d.pdf
rm building/doc_data.txt

#combine into blocks of 16 pages, inserting blanks at the end
echo "" | ps2pdf -sPAPERSIZE=a4 - building/blank.pdf

for ((n=1,block=1; n<=$PAGES; n+=16,block++))
do
	for ((i=$n; i<=$n + 15; i++))
	do
		if ((i <= PAGES))
		  then
			FILES="$FILES building/pg_$i.pdf"
		else
			FILES="$FILES building/blank.pdf"
		fi
	done
	echo "creating block $block"
	pdftk $FILES output building/block_$block.pdf
	FILES=
done
rm building/pg_*.pdf
rm building/blank.pdf

#put each block into the correct order and rotation
for ((n=1,block=1; n<=$PAGES; n+=16,block++))
do
	echo "reordering and rotating block $block"
	pdftk building/block_$block.pdf cat 5south 12south 4 13 11south 6south 14 3 7south 10south 2 15 9south 8south 16 1 output building/block_$block_reordered.pdf
	rm building/block_$block.pdf
	mv building/block_$block_reordered.pdf building/block_$block.pdf
done

#transform each block into a 4up signature
for ((n=1,block=1; n<=$PAGES; n+=16,block++))
do
	echo "creating 4up signature $block"
	pdfnup -q --nup 2x2 --batch --no-landscape --outfile building/block_$block-4up.pdf building/block_$block.pdf
	rm building/block_$block.pdf
	mv building/block_$block-4up.pdf building/sig_$block.pdf
done

#merge signatures into single PDF
echo "merging signatures"
FILES=
for ((n=1,block=1; n<=$PAGES; n+=16,block++))
do
	FILES="$FILES building/sig_$block.pdf"
done
rm $1
pdftk $FILES cat output $1

rm -rf building
